const { OK } = require("http-status");

module.exports = class Controller {
  ok(res, next) {
    return data => {
      res.status(OK).json(data);
      next();
    };
  }
};
