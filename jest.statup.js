const jestCli = require("jest-cli");

const server = require("./server/server");

const beforeAllTests = async () => {
  try {
    await server.bootstrap();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

const afterAllTests = () => {
  return server.shutdown();
};

beforeAllTests()
  .then(() => jestCli.run())
  .then(() => afterAllTests())
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
