require("dotenv").config({ path: ".env" });
const jwt = require("jsonwebtoken");

const environment = require("../config/environment");

function verifyToken(req, res, next) {
  var token = req.headers["authorization"];
  if (!token)
    return res
      .status(500)
      .send({ message: "No tokens were sentenhum token foi enviado." });
  if (token.startsWith("Bearer ")) {
    token = token.slice(7, token.length);
  }
  jwt.verify(token, environment.application.secret, function(err, decoded) {
    if (err)
      return res.status(500).send({ message: "Failed to authenticate token." });
    req.auth = true;
    next();
  });
}

module.exports = verifyToken;
