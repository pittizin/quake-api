const request = require("supertest");

const address = global["address"];

async function getGamesReport() {
  const res = await request(address)
    .get("/games")
    .set(
      "Authorization",
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.kk5T2cf7dR_1lDq2mUXKZwi4okQIz6zonaWCT5elMz8"
    );

  return res.body;
}

module.exports = { getGamesReport };
