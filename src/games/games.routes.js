const express = require("express");
const router = express.Router();

const gamesController = require("./games.controller");
const verifyToken = require("../../server/security.middleware");

router.get("/", verifyToken, gamesController.gameParser());

module.exports = router;
