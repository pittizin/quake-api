const request = require("supertest");

const address = global["address"];

describe("check api health", () => {
  it('You must return a 200 code status and a "pong" message', async () => {
    const res = await request(address).get("/ping");
    expect(res.statusCode).toEqual(200);
    expect(res.text).toEqual("pong");
  });
});
